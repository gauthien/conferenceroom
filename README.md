# ConferenceController

ConferenceController presents a simple interface for controlling AV 
equipment in our conference room.

## Installation

To install:

```
$ cd installation_directory
$ npm install
$ npm install webpack
$ npm install -g bunyan
$ npm start | bunyan -o short
```

### Important: the install of serialport will fail if run as sudo, so the
### npm install must be run as the standard user (pi).

Note that to bootstrap the project initially, I executed:

```
npm install -g express-generator
express --ejs conferenceroom
mv conferenceroom/* .
rmdir conferenceroom/
```

## Configuration

The only environment variable that is required on the system is NODE_ENV, 
which can be set either to:

```
export NODE_ENV=hackathon-development
export NODE_ENV=hackathon-production-hackathon
export NODE_ENV=conference-room-development
export NODE_ENV=conference-room-production
```

## Acknowledgements

1. Acknowledge Font awesome for the power-button.


