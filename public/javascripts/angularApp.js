
var app = angular.module('conferenceroom', ['ngMaterial']);

app.controller('MainCtrl', ['$scope', '$log', '$timeout', 'socket', function($scope, $log, $timeout, socket){
  $scope.title = 'LC9 Hackathon Room';
  
  $scope.clientModel = null;
  $scope.serverModel = null;
  $scope.intermediateModel = null;
  
  /*
   * Helper functions
   */
  $scope.getDevice = function(devicekey, isServer){
    var modelToUse = $scope.clientModel;
    if (isServer) { modelToUse = $scope.serverModel; }
    if (!modelToUse) { return undefined; }//not yet initialized
    return modelToUse[devicekey];
  };
  
  $scope.selectedCard = 'hackathon-front-conference-displays';
  $scope.getFlex = function(id){
    if (id === 'conference-room-display'){ //conference room always only has 1 card
      return 100;
    }
    if (id === $scope.selectedCard){ //hackathon room selected should be large
      return 50;
    }
    return 25;
  };
  

  /*
   * The code below will attempt to handle sync issues with the server and client
   * as the request to update will not be immediate and may clash with a users
   * current request (e.g., physical switch pushed while user attempts to set the
   * value through server). Here is the general outline of the flow:
   *    - 2 models are kept: $scope.clientModel and $scope.serverModel
   *    - Each of these models are an object that contain a set of devices (keys
   *      are the devicekey and values are the actual serialized devices).
   *    - When the client wants to change a devices model property, they will
   *      execute the 'setDeviceState' function. This function will:
   *        - add a property to that device's model saying that the device
   *          is being updated i.e., set from updatingPropertyName(property)
   *        - use socket.io to emit 'setdevicestate' in order to inform the
   *          server of the requested change.
   *    - On the client, we keep track of the state by adding a $watch to every
   *      device that uses the function 'deviceSyncStatus' to determine at all
   *      times whether a device is in sync.
   *    - The UI will be aware of the device's status as all model values are
   *      accessed via the getDeviceModelState function, which will return an
   *      object containing 2 or 3 properties:
   *        {isUpdating: false, value: 123} or 
   *        {isUpdating: true, valueWas: 123, value: 456}
   * 
   */
  //deal with local updates
  var updatingPropertyName = function(property) { return '_updating_' + property; }
  
  //return the sync status of a particular device.
  //returns 'insync', 'updating' or 'outofsync'
  $scope.deviceSyncStatus = function(devicekey){
    if(!devicekey){ return undefined; }
    
    var clientDeviceModel = $scope.clientModel[devicekey].model;
    var serverDeviceModel = $scope.serverModel[devicekey].model
    
    //check if the entire object tree is the same. If so, they are 'insync'.
    if (_.isEqual(clientDeviceModel, serverDeviceModel)){ return 'insync'; }
    
    //object tree is not insync. Is it being updated?
    for (var property in clientDeviceModel){ //iterates the object properties. don't use forEach
      var updatingPN = updatingPropertyName(property);
      
      if ( clientDeviceModel[updatingPN] ){
        
        //a request has been made to update. is the request complete?
        if (serverDeviceModel[property] === clientDeviceModel[updatingPN].value){
          //server now matches client model for at least this property
          //remove the updating flag and recursively check
          clientDeviceModel[property] = clientDeviceModel[updatingPN].value;
          delete clientDeviceModel[updatingPN];
          return $scope.deviceSyncStatus(devicekey); //redo until all flags are finished
        }
        else if (clientDeviceModel[updatingPN].timestamp + 5000 > new Date()){
          //give up after 5 sec or so and just revert to server
          //TODO: log error?
          return 'outofsync';
        }
        //still waiting for server to update.
        return 'updating';
      }
    };
    
    //object is not being updated from this client. we are simply 'outofsync'
    return 'outofsync';
  };
  
  //helper function to provide UI with device state and info about whether it is being updated
  $scope.getDeviceModelState = function(devicekey, property){
    if (!$scope.getDevice(devicekey)){ return; } //not yet initialized
    var clientDeviceModel = $scope.getDevice(devicekey).model;
    var toreturn = { isUpdating: false, value: clientDeviceModel[property] };
    if (clientDeviceModel[ updatingPropertyName(property) ]){
      toreturn = {
        isUpdating: true,
        valueWas: clientDeviceModel[property],
        value: clientDeviceModel[ updatingPropertyName(property) ].value
      };
    }
    return toreturn;
  };
  
  //a function to tell the server to change the device status
  $scope.setDeviceState = function(devicekey, property, newvalue){
    var serverDevice = $scope.getDevice(devicekey, true); 
    if (serverDevice.model[property] === newvalue){ return; } //don't call if it is already set
    
    //indicate on the local client model that we are updating this property
    var clientDevice = $scope.getDevice(devicekey);
    clientDevice.model[ updatingPropertyName(property) ] = {
      value: newvalue,
      timestamp: new Date()
    }
    
    //new value to set on the server
    socket.emit('setdevicestate', { 
        devicekey: devicekey,
        set: property, 
        value: newvalue
      }
    );
  };
  
  
  /*
   * 
   * Initialize the code and watch for model sync changes.
   *
   */
  socket.on('init', function (data) {
    $log.debug('init: data received from server:', data);
    $scope.clientModel = _.cloneDeep(data);
    $scope.serverModel = data;
    
    //add watchers to each device in the model that will notify of server changes
    _.each($scope.clientModel, function(dev){
      $scope.$watch('deviceSyncStatus("'+dev.devicekey+'")', function(newValue, oldValue){
        $log.debug('deviceSyncStatus new value = ' + newValue);
        if (newValue === 'outofsync'){
          //update directly the client
          $log.debug('The model is completely out of sync from the server. Updating');
          $scope.clientModel[dev.devicekey] = _.cloneDeep($scope.serverModel[dev.devicekey]);
        }
        else if(newValue === 'updating'){
          //just waiting for the update to propogate. A new model on the server will override
          $log.debug('updating model from the server');
        }
      });
    });
    
  });

  
  socket.on('modelchange', function (data) {
    $log.info('modelchange: server data now=', _.clone(data));
    $log.debug('modelchange: local data is=', $scope.clientModel);
    $scope.serverModel = data;
  });
  
}]);


/*
 * 
 * Factories
 * 
 */

app.factory('socket', ['$rootScope', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
    	console.log('an event occurred:' + eventName);
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
}])


/*
 * 
 * Directives
 * 
 */

/**
 * This directive represents an svg image that shows what device is being displayed on
 * a given display.
 */
.directive('confDisplayStackSvg', ['$log', function($log) {
  return {
    restrict: 'E',
    templateUrl: 'partials/displaystack.tpl.svg',
    templateNamespace: 'svg',
    scope:{
      showHangouts: '=',
      showTableHdmi: '=',
      showMicrosoft: '=',
      showChrome: '=',
      showAppletv: '='
    },
    replace:true,
    link: function(scope, element, attrs) {

    }
  };
}])

/**
 * Surface hub svg
 */
.directive('confSurfacehubSvg', ['$log', function($log) {
  return {
    restrict: 'E',
    templateUrl: 'partials/surfacehub.tpl.svg',
    templateNamespace: 'svg',
    scope:{
      surfacehubDisplay: '=',
      surfacehubHdmiswitch: '='
    },
    replace:true,
    link: function(scope, element, attrs) {
    }
  };
}])

/**
 * Information radiators svg
 */
.directive('confRadiatorsSvg', ['$log', function($log) {
  return {
    restrict: 'E',
    templateUrl: 'partials/informationradiators.tpl.svg',
    templateNamespace: 'svg',
    scope:{
      topRadiatorDisplay: '=',
      topRadiatorHdmiswitch: '=',
      bottomRadiatorDisplay: '=',
      bottomRadiatorHdmiswitch: '='
    },
    replace:true,
    link: function(scope, element, attrs) {
    }
  };
}])

/**
 * Front displays svg
 */
.directive('confFrontDisplaysSvg', ['$log', function($log) {
  return {
    restrict: 'E',
    templateUrl: 'partials/frontdisplays.tpl.svg',
    templateNamespace: 'svg',
    scope:{
      leftFrontDisplay: '=',
      leftFrontHdmiswitch: '=',
      rightFrontDisplay: '=',
      rightFrontHdmiswitch: '='
    },
    replace:true,
    link: function(scope, element, attrs) {
    }
  };
}])
