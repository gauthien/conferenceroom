
var bunyan = require('bunyan');

var env = process.env.NODE_ENV;
if (!env || (env !== 'hackathon-production' && 
             env !== 'conference-room-production' &&
             env !== 'hackathon-development' &&
             env !== 'conference-room-development')){
  if (env){
    console.log(
      '\033[31mWARNING: NODE_ENV set to invalid value "'+env+
      '". Defaulting to "hackathon-production". '+
      'Please add "export NODE_ENV=hackathon-production" (or similar) to '+
      '~/.profile'
    );
  }
  else{
    console.log(
      '\033[31mWARNING: No NODE_ENV set. Defaulting to "hackathon-production".'+
      'Please add "export NODE_ENV=hackathon-production" (or similar) to '+
      '~/.profile'
    );
  }
  env = 'hackathon-production';
}

var logger = bunyan.createLogger({
  name: env,
  streams: [
    { level: 'info', stream: process.stdout },
  ]
});

// export function for listening to the socket
module.exports = function (configuration) {
  logger.trace('configuration module.exports called');
  return {
    poll_frequency_ms: 1000,
    command_failed_time_ms: 2000,
    environment: env,
    logger: logger,
    
    HACKATHON_ROOM: 'hackathon-production',
    CONFERENCE_ROOM: 'conference-room-production',

    HACKATHON_ROOM_DEVELOPMENT: 'hackathon-development',
    CONFERENCE_ROOM_DEVELOPMENT: 'conference-room-development'
  };
};
