
var _ = require('lodash');
var serialport = require('serialport');
var readline = serialport.parsers.Readline;
var bytelength = serialport.parsers.ByteLength;
var regex = serialport.parsers.Regex;
var configuration = require('./configuration')();

/******************************************************************
 * 
 * The device driver for communication with a SmartAVI HDN-8P HDMI/USB
 * Switch. Implements the required API defined in device.js.
 * 
 * Documentation is poor, but the little that there is and empirical testing has shown
 * that commands sent to the HDN-8P are in the form: 
 *     //m[port #]\r   -switch KVM, USB, and audio to the specified port e.g., //3\r
 *                     -does not result in any return value
 *     //q\r           -query for the current port.
 *                     -returns exactly two lines: 
 *                          0[port #]   --the current port (e.g., 03 means port 3)
 *                          0D          --no idea what this means
 *                          
 * In addition, physically switching the device also results in a single line being sent 
 * through the RS-232 port. For example:
 *     Change channel to 3
 *     Change channel to 2
 *     
 * UPDATE: we bought a second box and the RS232 has a slightly different communication
 * behavior. Commands are the same, but responses are different. Differences:
 *   NEW BOX:
 *     - all data received:
 *         'RS232 switching to client-7 Keyboard & Mouse, Video, USB 2.0, Audio\r'
 *         'This unit changing to channel 7\r'
 *         'This unit changing to channel 3\r'
 *         'Computer (or slave) 5 is connected to transmitter\r'
 *         'recoverable TIMEOUTS = 16\r'
 *         'Non-recoverable TIMEOUTS = 1\r'
 *         '1'
 *   
 *     1) has an incessently blinking light
 *     2) changing channels via rs232 results in a reply of 2 lines:
 *         'RS232 switching to client-7 Keyboard & Mouse, Video, USB 2.0, Audio' (line 1)
 *         'This unit changing to channel 7' (line 2)
 *     3) changing channels physically results in a single line reply:
 *         'This unit changing to channel 3'
 *     4) other things that are received:
 *         'recoverable TIMEOUTS = 16'
 *         'Non-recoverable TIMEOUTS = 1'
 *     4) queries return NO line, just the port number (e.g., '7')
 *   OLD BOX: 
 *     1) no blinking light
 *     2) changing channels via rs232 does not result in a reply
 *     3) changing channels physically results in a single line reply (for port 7):
 *         'Change channel to 7'
 *     4) queries return two lines (02\r0d\r) / 2 lines of 8 bytes I think:
 *         '07' (line 1) and '0d' (line 2)
 *         
 *         
 *         
 *  ** Determined that the byte parser sometimes returns more than one byte. This probably
 *     has to do with the speed of which data are received.
 *  
 *  - NEW Proposal:
 *     - use a single byte parser 
 *     - immeditally push all of the data onto a 100 byte queue.
 *     - search for the last return carriage. it was all numbers 1-8 before the return
 *       carriage, then there is a new port.
 *         
 *  - OLD Proposal:
 *      - use a parser that returns ever byte
 *      - look for any number between 1-8
 *          - this will incorrectly register on: RS232, USB 2.0
 *      - new byte read:
 *          - always insert new byte into an array of the last 5 bytes read
 *          - if the new byte is 1, 4, 5, 6, 7, 8 the port has definitely changed.
 *          - if the new byte is 3 the port has changed UNLESS:
 *              - the byte from 2 reads ago is S
 *          - if the new byte is 2 the port has changed UNLESS:
 *              - the previous read byte is S OR 
 *              - the byte from 3 previous reads is S OR
 *              - the previous read byte is a space and the byte before that was B
 * 
 ******************************************************************/
module.exports = function (hdmiusbswitch_smartavi_HDN8P) {
  var logger = configuration.logger;
  var drivername = 'SmartAVI HDN-8P';
  logger.info('%s module.exports.', drivername);
  
  return function(namekey, driverData){
    /*
     * 
     * HELPER FUNCTIONS
     * 
     */
    var commandFromTask = function(task){
      var toreturn = '//q\r'; //query
      if (task.type === 'write'){
        toreturn = {
          cmd: '//m'+task.value+'\r',
          immediateCompletion: true
        }
      }
      return toreturn;
    };
    
    
    /*
     * 
     * ADDITIONAL VARIABLES REQUIRED TO CREATE A DEVICE
     * 
     */
    
    var parser = new readline();
    if (driverData.isNewAPI){
      parser = new bytelength({length: 1});
    }
    
    //all parameters for the port
    var portParams = {
      baudRate: 9600,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      parser: parser
    };
    
    
    //poll for power and input
    var pollingTaskset = [
      {name: 'input', type:'read'}
    ];
    
    
    //model is considered initialized if it contains an input attribute
    var isModelInitializedFn = function(model){
      return _.isNumber(model.input);
    };
    
    //a list that always holds the last 3 processed bytes in order
    //  (idx0 = last read, idx1 = 2 reads ago, idx2 = 3 reads ago)
    var dataHistory = _.fill(Array(100), '\r').join('');
    
    //function for reading the data returned by the serial port. must return an
    //object that can contain 4 properties: 
    //      taskComplete:  boolean value as to whether the task input is finished.
    //      updatedKey:    the key for the model property to be set.
    //      updatedValue:  the value that should be set for the model[updatedKey] property.
    //      errorOccurred: if an error occurs, this object will be set and contain two
    //                     properties: isFatal, description
    var dataReceivedFn = function(data, task, currentModel){
      var toreturn = {
        taskComplete: false,
        updatedKey: undefined,
        updatedValue: undefined,
        errorOccurred: undefined
      };
      
      data = ''+data;
      logger.debug('%s data received: %s', namekey, data);

      dataHistory = dataHistory + data; //append characters onto end history
      dataHistory.substring(0, data.length); //remove equal number of characters from the front of the array
      
      if (driverData.isNewAPI){ //the new switch with the "difficult" api
        
        //     1) changing channels via rs232 results in a reply of 2 lines:
        //         'RS232 switching to client-7 Keyboard & Mouse, Video, USB 2.0, Audio' (line 1)
        //         'This unit changing to channel 7' (line 2)
        //     2) changing channels physically results in a single line reply:
        //         'This unit changing to channel 3'
        //     3) other things that are received:
        //         'recoverable TIMEOUTS = 16'
        //         'Non-recoverable TIMEOUTS = 1'
        //     4) queries return NO line, just the port number (e.g., '7')
        
        logger.debug('%s: dataHistory (trimmed): %s', namekey, dataHistory.trim());
        
        //task can be complete at the end of the rs232
        var trimmedHistory = dataHistory.trim();
        if (task.type === 'write' &&
            trimmedHistory.substring(trimmedHistory.length-31,  trimmedHistory.length-1) === 'This unit changing to channel '){
          toreturn.taskComplete = true; //process next command
          logger.info('%s: RS232 changed switch.', namekey);
        }
        else{
          //only update value on query responses
          var lastUsedNewlineIdx = _.lastIndexOf(dataHistory, '\r'); //the last used newline index 
          var stringSinceLastNewline = dataHistory.substring(lastUsedNewlineIdx+1).trim();
          var reg = /^[1|2|3|4|5|6|7|8|9]+$/;
          if (reg.test(stringSinceLastNewline)){ //check if all subsequent reads are numbers 1-8
            logger.debug('%s input info received: "%s"', namekey, data);
            toreturn.updatedKey = 'input';
            toreturn.updatedValue = +data[data.length-1];
            toreturn.taskComplete = true; //process next command
          }
        }
      }
      else{ //the original machine with an "easier" api
        if (data.indexOf('0D') !== -1){ //query finished processing
          logger.debug('%s command received: %s', namekey, data);
          toreturn.taskComplete = true; //process next command 
        }
        else{ 
          //e.g., 'Change channel to 7' or '07'--result of manual switch 
          //      change (not programmatic task)
          logger.debug('%s input info received: "%s"', namekey, data);
          
          //simple parse
          var manualswitch = false;
          var newinput = +data //if the data are '07', would result in newinput = 7
          if (data.indexOf('Change channel to ') !== -1){
            newinput = +data['Change channel to '.length];
            manualswitch = true;
          }
          
          if (!newinput){
            logger.error('%s no/invalid input provided in data received from switch: "%s"', namekey, data);
            toreturn.errorOccurred = {
                isFatal: false,
                errorDescription: 'The switch '+namekey+' returned unexpected data: '+data
            };
            toreturn.taskComplete = true; //process next command
          }
          else{
            //the abstract device that the input may have changed. It will compare and 
            //update/notify listeners if needed
            logger.debug('%s input command received: %s', namekey, data);
            toreturn.taskComplete = true;
            toreturn.updatedKey = 'input';
            toreturn.updatedValue = newinput;
          }
        } 
      }

      
      /*
      var possible_portnum = _.toInteger(data);
      var newportnum = undefined;
      if (_.indexOf([1,4,5,6,7,8], possible_portnum) !== -1){ //if the byte 1,4,5,6,7,8 the port changed.
    	newportnum = possible_portnum;
      }
      else if (possible_portnum === 3 && 
    		   dataHistory[1] !== 'S'){
    	newportnum = possible_portnum;
      }
      else if (possible_portnum === 2 &&
    		   dataHistory[0] !== 'S' &&
    		   dataHistory[2] !== 'S' &&
    		   (dataHistory[0] !== ' ' || dataHistory[1] !== 'B')){
    	newportnum = possible_portnum;
      }
      dataHistory.unshift(data);
      dataHistory.pop(); //remove last in the queue
      
      if (newportnum){
    	logger.debug('%s input info received: "%s"', namekey, data);
        toreturn.updatedKey = 'input';
        toreturn.updatedValue = newportnum;
	    toreturn.taskComplete = true; //process next command 
      }
      */
      return toreturn;
    };
    
    /*
     * 
     * The driver API
     * 
     */
    return {
      getCommandFromTask: commandFromTask,
      
      getPortParams: function(){
        return portParams;
      },
      
      getPollingTaskset: function(){
        return pollingTaskset;
      },
      
      dataReceived: function(data, task, currentModel){
        return dataReceivedFn(data, task, currentModel);
      },
      
      isModelInitialized: function(model){
        return isModelInitializedFn(model);
      }
    };
  };
};
