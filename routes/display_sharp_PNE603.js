
var _ = require('lodash');
var serialport = require('serialport');
var readline = serialport.parsers.Readline;
var async = require("async");
var configuration = require("./configuration")();

//export function for listening to the socket
module.exports = function (display_sharp_PNE603) {
  var logger = configuration.logger;
  logger.info('Startup Sharp TV PN-E603 module.exports.');

  //function for converting a task object to a serial port command
  var taskToCommand = function(task){
    if(task.name=='power' && task.type=='read'){
      return 'POWR????\r';
    }
    else if (task.name=='power' && task.type=='write'){
      if (task.value){ value=1; }
      else{ task.value=0; }
      return 'POWR000'+value+'\r';
    }
    else if (task.name=='input' && task.type=='read'){
      return 'INPS????\r';
    }
    else if (task.name=='input' && task.type=='write'){
      var value = '09'; //default to 'HDMI1'
      if (task.value == 'HDMI2'){ value='12'; }
      else if (task.value == 'DISPLAYPORT'){ value='14' }
      return 'INPS00'+value+'\r';
    }
    logger.error('Sharp TV PN-E603 invalid task specified: ', task);
  };

  /******************************************************************
   * 
   * 
   * Interface for communication with the sharp tv PN-E603
   * 
   * 
   ******************************************************************/
  var sharptv = function(name, interfaceid, initcallback){
    var namekey = name + ' [' + interfaceid + ']';
    logger.info('Initialize Sharp TV PN-E603 "%s"', namekey);
    
    //serialport interface for communication with the sharptv PN-E603
    var rawport = new serialport(interfaceid, {
      baudRate: 38400,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      parser: new readline()
      //parser: serialport.parsers.readline('\n')
    });
    
    //callback functions executed with model changes
    var listeners = [];
    var errorListeners = [];

    //model is simple object with two keys:
    //  'power': a integer 0 (off) or 1 (on)
    //  'input': a string 'HDMI1', 'HDMI2', or 'DISPLAYPORT'
    var model = null; 

    // The serialportDataReceived(data) is executed from the serialport.on('data')
    // function and is created within body of the task execution of the queue, allowing
    // it to have access to the task last executed and data returned. 
    // 
    // Although confusing, this workflow allows us to guarantee that only 
    // a single command-response is ever currently active on the serial port. 
    // 
    // Why not just setup "serialport.on('data')" from within this function?
    // Because the .on('data') method cannot be canceled and would end up being a
    // memory leak
    var serialportDataReceived = null;

    //Setup a queue for processing serial port commands. Note that
    //concurrency is set to 1, so any task must go to completion before
    //another task is processed.
    var q = async.priorityQueue(function (task, callback) { 

      //function called whenever there is data sent from the device's serial port
      serialportDataReceived = function(data){
        data = data.trim();
        logger.trace(
          'Sharp TV PN-E603 "%s":"%s.%s" received data "%s"', 
          namekey, task.name, task.type, data
        );
        
        var executeCallback = false;
        if(task.type=='read' && /^\+?\d+$/.test(data)){
          logger.debug('Sharp TV PN-E603 "%s" processed read task data: %s', namekey, data);
          data = parseInt(data);
          if (task.name == 'input'){
            if (data == 9){ data = 'HDMI1'; } //9 == HDMI1
            else if (data == 12){ data = 'HDMI2'; } //12 == HDMI2
            else if (data == 14){ data = 'DISPLAYPORT'; } //14 == DISPLAYPORT
          }

          if (!model){ 
            model = {power: null, input: null};
          }

          if (model[task.name] != data){
            logger.info(
              'Sharp TV PN-E603 "%s" status change: "%s.%s" was "%s" and is now "%s"', 
              namekey, task.name, task.type, model[task.name], data
            );
            var oldValue = model[task.name];
            model[task.name] = data;
            _.each(listeners, function(listenercallback){
              listenercallback(task.name, data, oldValue, model);
            });
          }
          executeCallback = true;
        }
        else if (!data || data.substring(0,4) != 'WAIT'){
          //Notify queue of completion. Only WAIT will postpone the queue. Note
          //that this does work well with many of the more complex behavior available
          //with serialport communication one can do with the sharp API such as
          //daisy chaining monitors (requires multiple interactions), etc. That said,
          //I'm hopeful that it is sufficient for our simple get/put requests.
          executeCallback = true;
          logger.debug('Sharp TV PN-E603 "%s" read task processed: "%s"', namekey, data);
        }
        
        //notify queue of completion
        if (executeCallback){
          logger.debug(
            'Sharp TV PN-E603 "%s" task "%s.%s" complete. Processed data: "%s"', 
            namekey, task.name, task.type, data
          );
          callback();
        }
      };
      
      //execute the command
      var cmd = taskToCommand(task);
      logger.debug('SharpTV PN-E603 "'+namekey+'" executing command ' + cmd);
      rawport.write(cmd);
      
      //catchall error to end task and allow other execution if the task doesn't 
      //complete (as expected).
      setTimeout(function(){
        try{
          callback();
          logger.error(
            'SharpTV PN-E603 "%s" command "%s.%s" did not complete in %sms. '+
            'Forced queue to continue processing.', 
            namekey, task.name, task.type, configuration.command_failed_time_ms
          );
        } catch (ex) {
          //this is expected--if the task completes successfully and callback
          //was previously executed, than calling callback() will throw an
          //exception.
        }
      }, configuration.command_failed_time_ms);
      
    }, 1); //1 = concurrency of 1

    //listen for responses from TV
    rawport.on('data', function (data) {
      if (serialportDataReceived){
        serialportDataReceived(data);
      }
    });

    //push a request to read the power and input status on the current TV
    var readstatusFn = function(){
      q.push({name: 'power', type:'read'}, 10);
      q.push({name: 'input', type:'read'}, 10);
    };

    //startup initial status reading
    rawport.on('open', function(data){ 
      logger.info('Sharp TV PN-E603 "%s" port opened.', namekey);
      readstatusFn();
    });
    
    // anytime all the queue is finished, execute read status 
    q.drain = function(){
      setTimeout(readstatusFn, configuration.poll_frequency_ms); 
    };
    
    //startup or other read errors
    rawport.on('error', function(err) {
      logger.error('Error from Sharp TV PN-E603 port "%s": ', namekey, err.message);
      _.each(errorListeners, function(listenercallback){
        listenercallback(
          true, 'Error from Sharp TV PN-E603 port "'+namekey+'": ' + err.message
        );
      });
    });

    return {
      /* 
       * Any change to the tv ('input' or 'power') will execute the callback 
       * provided. This callback function should have the signature: 
       *    fn(changetype, newvalue, oldvalue, model)
       * 
       * where: 'changetype' will be 'input' or 'power'
       *        'newvalue' will contain the value of the new setting
       *                      (integers 0 or 1 for 'power')
       *                      ('HDMI1', 'HDMI2', or 'DISPLAYPORT for 'input')
       *        'oldvalue' will contain the previously set value
       *        'model' will contain the full model object, which has two
       *                keys 'input' and 'power'
       * 
       * returns: a function that, when executed, will remove the callback from
       *          future notifications.
       */
      onChange: function(callback){
        listeners.push(callback);
        return function(){
          listeners = _.reduce(listeners, function(memo, item){
            if (item != callback){ memo.push(item); }
            return memo;
          }, []);
        };
      },
      
      /*
       * Any detected error will result in this callback being executed.
       * The callback function should have the signature: 
       *   fn(errorStatus, errorTxt)
       * 
       * where: 'isFatal' a boolean describing whether the error is un-recoverable.
       *        'errorTxt' will be a user readable string describing the error.
       *        
       * returns: a function that, when executed, will remove the callback from
       *          future notifications.
       */
      onError: function(callback){
        errorListeners.push(callback);
        return function(){
          errorListeners = _.reduce(errorListeners, function(memo, item){
            if (item != callback){ memo.push(item); }
            return memo;
          }, []);
        };
      },

      /*
       * Turn the power on or off (0 for off, 1 for on, default=1)
       */
      setPower: function(value){
        if (value !== 0 && value !== 1){
          logger.error(
            'Sharp TV PN-E603 "%s": Power value must be 0 or 1 not "%s".', namekey, value
          );
        }
        else{
          logger.info('Sharp TV PN-E603 "%s": Setting power to "%s".', namekey, value);
          q.push({name: 'power', type:'write', value:value}, 0);
        }
      },

      /*
       * Set the input port (HDMI1, HDMI2, DISPLAYPORT. Default=HDMI1)
       */
      setInput: function(value){
        if (value !== 'HDMI1' && value !== 'HDMI2' && value !== 'DISPLAYPORT'){
          logger.error(
            'Sharp TV PN-E603 "%s" setInput error: Input value must be "HDMI1", HDMI2"'+
            ' or "DISPLAYPORT" not %s.', namekey, value
          );
        }
        else{
          logger.info('Sharp TV PN-E603 "%s": Setting input to %s.', namekey, value);
          q.push({name: 'input', type:'write', value:value}, 0);
        }
      }
    };
  };

  //
  //API
  //
  var all_tvs = {}; //keep track of all tvs
  return {
    /*
     * get an instance of the sharptv. If provided, initcallback will be
     * called when the model is initialized and will be passed the state of 
     * the tv.
     */
    getDevice(name, interfaceid, initcallback){
      var key = name+'['+interfaceid+']';
      if (all_tvs[key]){
        var toreturn = all_tvs[key];
        if (initcallback){ 
          initcallback(toreturn.model); 
          initcallback=null;
        }
        return toreturn;
      }
      toreturn = sharptv(name, interfaceid, initcallback);
      all_tvs[key] = toreturn;
      return toreturn;
    }
  }
};
