
var _ = require('lodash');
var serialport = require('serialport');
var bytelength = serialport.parsers.ByteLength;
var configuration = require('./configuration')();


/******************************************************************
 * 
 * Interface for communication with the samsung tvs QM55F and QM65F
 * 
 * Note: Samsung has a weird HEX language for communicating with their
 *       devices. 
 *       
 *       All communications take place in hexadecimals. The checksum 
 *       is calculated by adding up all values except the header. If 
 *       a checksum adds up to be more than 2 digits as shown below 
 *       (11+FF+01+01=112), the first digit is removed.
 *         = Power On & ID=0
 *       
 *    Here is an example of how to send a power on command:
 *       
 *    power on: [0xAA,0x11,0xFE,0x01,0x01,0x11]
 *        where 0x11 is the Command Type "Power control"
 *              0xFE is the Device ID (0xFE means "all devices")
 *              0x01 is the data length I think
 *              0x01 is the data value (1 means on)
 *              0x11 is the checksum calculated as described above   
 * 
 ******************************************************************/
module.exports = function (display_samsung_QM55F_QM65F) {
  var logger = configuration.logger;
  var drivername = 'Samsung QM55F/QM65F TV';
  logger.info('%s module.exports.', drivername);

  return function(namekey){
    
    /*
     * 
     * HELPER FUNCTIONS
     * 
     */
    
    // taken from https://github.com/phillipsnick/samsung-tv
    // This function will return the checksum of the serial port command
    // sent to the samsung tv. 
    //    hexarr: an array of hex values of length 5
    //    returns: a string containing the sum of the array, converted to
    //             hexadecimal and dropping the 1st digit if there are more
    //             than 2 digits
    //         note: to compare with decimal: parseInt(calculateChecksum(hexarr), 16)
    var calculateChecksum = function(hexarr){
      return (_.sum(hexarr)).toString(16).slice(-2);
    };
    
    
    /*
    var calculateChecksum2 = function(array) {
      var sum = 0;

      array.forEach(function(element, index) {
        sum+= element;
      });

      return (sum).toString(16).slice(-2);
    };*/
    
    
    //
    //hex stuff
    //
    var hexToTaskname = function(hexcode){
      if (hexcode == 0x11) { return 'power'; }
      if (hexcode == 0x14) { return 'input'; }
      return undefined;
    };
    var tasknameToHex = function(taskname){
      if (taskname === 'power') { return 0x11; }
      if (taskname === 'input') { return 0x14; }
      return undefined;
    };
    var tasktypeToHex = function(tasktype){
      if (tasktype === 'read') { return 0x00; }
      if (tasktype === 'write') { return 0x01; }
      return undefined;
    };
    var _inputStringHexcodesHM = {
      //Empirically, changes to HDMI3, HDMI4, and DisplayPort2 are not reported. Stupid.
      'HDMI1': 0x22, 'HDMI2': 0x24, 'DVI': 0x18, 'PC': 0x14, 'DisplayPort': 0x25
      
      // BELOW ARE DOCUMENTED CODES. THEY ARE WRONG. ABOVE WERE MANUALLY DETERMINED
      //'PC': 0x14, 'DVI': 0x18, 'Input source': 0x0C, 'Component': 0x08, 'MagicInfo': 0x20,
      //'DVI_video': 0x1F, 'RF(TV)': 0x30, 'DTV': 0x40, 'HDMI1': 0x21, 'HDMI1_PC': 0x22,
      //'HDMI2': 0x23, 'HDMI2_PC': 0x24, 'DisplayPort': 0x25
    }
    var inputStringToHex = function(str){ return _inputStringHexcodesHM[str]; };
    var hexToInputString = function(hexcode){ 
      return _.reduce(_.keys(_inputStringHexcodesHM), function(memo, key){
        if (_inputStringHexcodesHM[key] === hexcode){ memo = key; }
        return memo;
      }, undefined);
    };
    
    //
    // Inform device to abort a task if certain conditions are not met. In this case
    // if the power isn't on, don't query for input as it just returns an error.
    //
    var abortTask = function(task, model){
      if (task.name !== 'power' && model.power !== 1) { return true; }
      return false;
    };
    
    
    //
    // convert a task to the actual command to be executed on the serial port
    //
    var commandFromTask = function(task){
      var toreturn = null;
      var tvid = task.tvid ? task.tvid : 0x01; 
      //0xFF; //0xFF means device zero for some reason, 0xFE means all devices.
      
      toreturn = [tasknameToHex(task.name), tvid, tasktypeToHex(task.type)];
      
      if (_.isUndefined(toreturn[0]) || _.isUndefined(toreturn[2])){ //invalid task?
        logger.error('Invalid task passed to commandFromTask: %j', task);
        return;
      }
      
      if (task.type === 'write'){
        toreturn.push((task.value).toString(16));
      }
      toreturn.push( '0x'+calculateChecksum(toreturn) );
      toreturn.splice(0, 0, 0xAA); //add the header
      
      //works for power:
      //ON: 
      //toreturn = new Buffer([0xAA, 0x11, 0xff, 0x01, 0x01, '0x'+calculateChecksum([0x11, 0xff, 0x01, 0x01])]);
      //OFF:
      //toreturn = new Buffer([0xAA, 0x11, 0xff, 0x01, 0x00, '0x'+calculateChecksum([0x11, 0xff, 0x01, 0x00])]);
      
      //QUERY STATE, which equals: 170, 17, 255, 0, 16:
      //toreturn = new Buffer([0xAA, 0x11, 0xff, 0x00, '0x'+calculateChecksum([0x11, 0xff, 0x00])]);
      
      //170, 17, 254, 0, 0x0f
      logger.trace('%s commandFromTask being returned is buffer of: %s', namekey, toreturn.join(", "));
      return new Buffer(toreturn);
    };

    /*
     * 
     * VARIABLES REQUIRED TO CREATE A DEVICE
     * 
     */
    
    //all parameters for the port
    var portParams = {
      baudRate: 9600,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      parser: new bytelength({length: 8})
      //parser: serialport.parsers.byteLength(8)
    };
    
    //poll for power and input
    var pollingTaskset = [
      {name: 'power', type:'read'},
      {name: 'input', type:'read'}
    ];
    
    //model is considered initialized if it contains a power and input attribute
    var isModelInitializedFn = function(model){
      if (_.isNumber(model.power) && _.has(_inputStringHexcodesHM, model.input)){
        return true;
      }
      return false;
    };
    
    //function for reading the data returned by the serial port. must return an
    //object that can contain 4 properties: 
    //      taskComplete:  boolean value as to whether the task input is finished.
    //      updatedKey:    the key for the model property to be set.
    //      updatedValue:  the value that should be set for the model[updatedKey] property.
    //      errorOccurred: if an error occurs, this object will be set and contain two
    //                     properties: isFatal, description
    var dataReceivedFn = function(data, task, currentModel){
      var toreturn = {
        taskComplete: false,
        updatedKey: undefined,
        updatedValue: undefined,
        errorOccurred: undefined
      };
      
      //data are an array of hex bytes
      logger.trace('%s data received: [%s]', namekey, data.join(", "));
      
      //
      //check for errors
      //
      var errortxt = undefined;
      if (data[7] !== parseInt(calculateChecksum(data.slice(1, 7)), 16) ){
        errortxt = 'Invalid checksum returned: ' + data[7] + 
                   ' (expected '+parseInt(calculateChecksum(data.slice(1, 7)), 16)+')';
      }
      if (data[0] !== 170){ 
        errortxt = 'Unexpected header returned: ' + data[0];
      }
      if (data[1] !== 255){ 
        errortxt = 'Unexpected command returned: ' + data[1];
      }
      //if (data[2] !== 255){ 
      //  //not sure why this isn't the TV id (0), but whatever
      //  errortxt = 'Unexpected ID returned: ' + data[2]; 
      //}
      if (data[4] != 65 && data[4] != 78){ //65 is 'A' in unicode, 78 is 'N'
        errortxt = 'Unexpected Ack/Nak returned: ' + data[4]; 
      } 
      if (data[4] === 78){
        errortxt = 'Error code Nak returned for '+hexToTaskname(data[5])+
                   ' command ('+data[5]+'). Error code: ' + data[6];
      }
      
      //report errors if there were any
      if (errortxt){
        logger.warn('%s: %s :: full data=[%s]', namekey, errortxt, data.join(", "));
        toreturn.errorOccurred = {
          isFatal: false,
          errorDescription: 'Invalid data received from display "'+namekey+'": '+errortxt
        };
        return toreturn;
      }
      
      //should be error free. parse and update model if necessary
      
      //valid data probably
      if (hexToTaskname(data[5]) === 'power'){
        toreturn.updatedKey = 'power';
        toreturn.updatedValue = data[6];
      }
      else if (hexToTaskname(data[5]) === 'input'){
        toreturn.updatedKey = 'input';
        toreturn.updatedValue = hexToInputString(data[6]);
      }
      
      //mark task complete if it is the same as the acknowledged task. Might always be the case??
      if (task.name === toreturn.updatedKey){
        toreturn.taskComplete = true;
      }
      return toreturn;
    };

    /*
     * 
     * The driver API
     * 
     */
    return {
      abortTask: abortTask,
      getCommandFromTask: commandFromTask,
      
      getPortParams: function(){
        return portParams;
      },
      
      getPollingTaskset: function(){
        return pollingTaskset;
      },
      
      dataReceived: function(data, task, currentModel){
        return dataReceivedFn(data, task, currentModel);
      },
      
      isModelInitialized: function(model){
        return isModelInitializedFn(model);
      }
    };
  };
};
