
var _ = require('lodash');
var serialport = require('serialport');
var readline = serialport.parsers.Readline;
var configuration = require('./configuration')();

/******************************************************************
 * 
 * The device driver for communication with the microsoft surfacehub.
 * The documentation is available on the microsft website:
 *   https://technet.microsoft.com/en-us/itpro/surface-hub/use-room-control-system-with-surface-hub
 * 
 ******************************************************************/
module.exports = function (hdmiusbswitch_smartavi_HDN8P) {
  var logger = configuration.logger;
  var drivername = 'Microsoft Surface Hub';
  logger.info('%s module.exports.', drivername);
  
  return function(namekey, driverData){
    /*
     * 
     * HELPER FUNCTIONS
     * 
     */
    
    var abortTask = function(task, model){
      if (task.name !== 'power' && model.power !== 1) { return true; }
      return false;
    };
    
    var commandFromTask = function(task){
      var toreturn; //query
      if (task.name === 'power') { 
        toreturn = 'Power?';
        if (task.type === 'write'){
          toreturn = 'PowerOff';
          if (task.value){
            toreturn = 'PowerOn';
          }
        }
      }
      else if (task.name === 'input') { 
        toreturn = 'Source?';
        if (task.type === 'write'){
          toreturn = 'Source='+task.value;
        }
      }
      toreturn = toreturn + '\r';
      return toreturn;
    };
    
    
    /*
     * 
     * ADDITIONAL VARIABLES REQUIRED TO CREATE A DEVICE
     * 
     */
    
    //all parameters for the port
    var portParams = {
      baudRate: 115200,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      parser: new readline()
      //parser: serialport.parsers.readline('\r')
    };
    
    
    //poll for power and input
    var pollingTaskset = [
      {name: 'power', type:'read'},
      {name: 'input', type:'read'}
    ];
    
    
    //model is considered initialized if it contains an input attribute
    var isModelInitializedFn = function(model){
      return _.isNumber(model.power) && _.isNumber(model.input);
    };
    
    
    //function for reading the data returned by the serial port. must return an
    //object that can contain 4 properties: 
    //      taskComplete:  boolean value as to whether the task input is finished.
    //      updatedKey:    the key for the model property to be set.
    //      updatedValue:  the value that should be set for the model[updatedKey] property.
    //      errorOccurred: if an error occurs, this object will be set and contain two
    //                     properties: isFatal, description
    var dataReceivedFn = function(data, task, currentModel){
      var toreturn = {
        taskComplete: false,
        updatedKey: undefined,
        updatedValue: undefined,
        errorOccurred: undefined
      };
      
      data = data.trim();
      logger.trace('%s data received (after trim): %', namekey, data);
      
      if (data.indexOf('Source=') !== -1 || data.indexOf('source=')){
        logger.info('%s source command received: %s', namekey, data);
        toreturn.updatedKey = 'input';
        toreturn.updatedValue = parseInt(data.slice(data.indexOf('Source=')+7));
        toreturn.taskComplete = true; //process next command 
      }
      else if (data.indexOf('Power=') !== -1 || data.indexOf('power=')){
        logger.info('%s power command received: %s', namekey, data);
        toreturn.updatedKey = 'power';
        toreturn.updatedValue = parseInt(data.slice(data.indexOf('Power=')+7));
        toreturn.taskComplete = true; //process next command 
      }
      else if (data.indexOf('Power?') === -1 && data.indexOf('Source?') === -1){
        logger.warn('%s unexpected command received: %s', namekey, data);
      }
      return toreturn;
    };
    
    /*
     * 
     * The driver API
     * 
     */
    return {
      abortTask: abortTask,
      
      getCommandFromTask: commandFromTask,
      
      getPortParams: function(){
        return portParams;
      },
      
      getPollingTaskset: function(){
        return pollingTaskset;
      },
      
      dataReceived: function(data, task, currentModel){
        return dataReceivedFn(data, task, currentModel);
      },
      
      isModelInitialized: function(model){
        return isModelInitializedFn(model);
      }
    };
  };
};
