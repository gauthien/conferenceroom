
var _ = require('lodash');
var serialport = require('serialport');
var configuration = require('./configuration')();
var device = require('./device')()
//var sharptvs = require('./display_sharp_PNE603')();
var iogear_switches = require('./hdmiswitch_iogear_GHSW8241')();
var smartavi_usb_switches = require('./hdmiusbswitch_smartavi_HDN8P')();
var samsungtvs = require('./display_samsung_QM55F_QM65F')();
var surfacehub = require('./display_microsoft_surfacehub')();

var logger = configuration.logger;



//output all the available serial interfaces to the command line at startup
serialport.list(function (err, ports) {
  var count = 1;
  logger.warn('socket.js: Startup found serial interfaces:');
  ports.forEach(function(port) {
    logger.warn(port, 'Serial interface %s:', count);
  	count += 1;
  });
});


/*
var port = new serialport('/dev/cu.usbserial', {
  baudRate: 9600,
  dataBits: 8,
  stopBits: 1,
  parity: 'none',
  //parser: serialport.parsers.readline('\n')
});

var stagger = function(i, alphabet){
  setTimeout(function(){ 
    var totest = alphabet[i].toUpperCase();
    console.log('testing "' + totest+'"');
    port.write('port'+totest);
    port.write('port'+totest);
    if (i+1 < alphabet.length){
      stagger(i+1, alphabet);
    }
  }, 500);
}

port.on('open', function() {
  port.write('status');
  port.write('status');
  //port.write('port0R');
  //port.write('port0R');
  //port.write('port0R');
  //port.write('port3R');
  var alphabet = 'abcdefghijklmnopqrstuvwxyz';
  stagger(0, alphabet);
});

// open errors will be emitted as an error event
port.on('error', function(err) {
  console.log('Error: ', err.message);
});

port.on('data', function (data) {
  console.log('Data: ' + data);
});
*/


/*
 * 
 * 
 * 
 * Load up all devices
 * 
 * 
 * 
 */
global.allDevices = {};
global.addDevice = function(key, dev){
  dev._key = key;
  global.allDevices[key] = dev;
};
global.serializeDevice = function(dev){
  return {
    devicekey: dev._key,
    type: dev.getInfo().type,
    manufacturer: dev.getInfo().manufacturer,
    modelnum: dev.getInfo().modelnum,
    location: dev.getInfo().location,
    model: dev.getModel()
  };
};

//
//helper functions for adding devices to our device set
//

//the usb switches.
var addSmartAVIUSBSwitch = function(location, interfaceId, isNewAPI){
  global.addDevice(location, device.getDevice(
    {
      type: 'usbswitch',
      manufacturer: 'smart-avi',
      model: 'HDN8P',
      location: location,
      interfaceId: interfaceId
    },
    { isNewAPI: isNewAPI },
    smartavi_usb_switches
  ));
};

//the hdmi switches.
var addIogearHDMISwitch = function(location, interfaceId){
  var key = 'hdmiswitch:'+location; //e.g., hdmiswitch:informationradiator:top, hdmiswitch:surfacehub
  global.addDevice(key, device.getDevice(
    { 
      type: 'hdmiswitch',
      manufacturer: 'iogear',
      model: 'GHSW8241',
      location: location,
      interfaceId: interfaceId
    },
    {},
    iogear_switches
  ));
};


//the displays
var addSamsungDisplay = function(location, model, interfaceId){
  var key = 'display:'+location; //e.g., display:informationradiator:top, display:surfacehub
  global.addDevice(key, device.getDevice(
    { 
      type: 'display',
      manufacturer: 'samsung',
      model: model,
      location: location,
      interfaceId: interfaceId
    },
    {},
    samsungtvs
  ));
};


//the surfacehub
var addSurfacehub = function(interfaceId){
  global.addDevice('display:surfacehub', device.getDevice(
    { 
      type: 'display',
      manufacturer: 'microsoft',
      model: 'surfacehub',
      location: 'frontcenter',
      interfaceId: interfaceId
    },
    {},
    surfacehub
  ));
};


//start adding the devices based on environment variable
if(configuration.environment === configuration.HACKATHON_ROOM_DEVELOPMENT){
  // /dev/ttyUSB0 = /dev/cu.usbserial-ST200897 = 1 :: DISPLAY IR TOP
  // /dev/ttyUSB1 = /dev/cu.usbserial-ST200898 = 2 :: DISPLAY IR BOTTOM
  // /dev/ttyUSB2 = /dev/cu.usbserial-ST200899 = 3 :: DISPLAY FRONT LEFT
  // /dev/ttyUSB3 = /dev/cu.usbserial-ST200900 = 4 :: DISPLAY FRONT RIGHT
  // /dev/ttyUSB4 = /dev/cu.usbserial-ST200901 = 5 :: DISPLAY SURFACEHUB
  // /dev/ttyUSB5 = /dev/cu.usbserial-ST200902 = 6 :: 
  // /dev/ttyUSB6 = /dev/cu.usbserial-ST200903 = 7 :: USBSWITCH KEYBOARD MOUSE
  // /dev/ttyUSB7 = /dev/cu.usbserial-ST200904 = 8 :: USBSWITCH CAMERA FLOOR (speakers)
  // /dev/ttyUSB8 = /dev/cu.usbserial-ST200905 = 9 :: 
  // /dev/ttyUSB9 = /dev/cu.usbserial-ST200906 = 10 :: 
  // /dev/ttyUSB10 = /dev/cu.usbserial-ST200907 = 11 :: HDMISWITCH IR TOP
  // /dev/ttyUSB11 = /dev/cu.usbserial-ST200908 = 12 :: 
  // /dev/ttyUSB12 = /dev/cu.usbserial-ST200909 = 13 :: HDMISWITCH IR BOTTOM
  // /dev/ttyUSB13 = /dev/cu.usbserial-ST200910 = 14 :: HDMISWITCH FRONT LEFT
  // /dev/ttyUSB14 = /dev/cu.usbserial-ST200911 = 15 :: HDMISWITCH FRONT RIGHT
  // /dev/ttyUSB15 = /dev/cu.usbserial-ST200912 = 16 :: HDMISWITCH SURFACEHUB
  
  addSamsungDisplay('informationradiator:top', 'QM65F', '/dev/cu.usbserial-ST200897');
  addSamsungDisplay('informationradiator:bottom', 'QM55F', '/dev/cu.usbserial-ST200898'); 
  addSamsungDisplay('front:left', 'QM65F', '/dev/cu.usbserial-ST200899');
  addSamsungDisplay('front:right', 'QM65F', '/dev/cu.usbserial-ST200900');
  addSurfacehub('/dev/cu.usbserial-ST200901');
  addIogearHDMISwitch('informationradiator:top', '/dev/cu.usbserial-ST200907');
  addIogearHDMISwitch('informationradiator:bottom', '/dev/cu.usbserial-ST200909');
  addIogearHDMISwitch('front:left', '/dev/cu.usbserial-ST200910');
  addIogearHDMISwitch('front:right', '/dev/cu.usbserial-ST200911');
  addIogearHDMISwitch('surfacehub', '/dev/cu.usbserial-ST200912');
  addSmartAVIUSBSwitch('usbswitch:keyboard_mouse', '/dev/cu.usbserial-ST200903', true);
  addSmartAVIUSBSwitch('usbswitch:camera_mics_speakers', '/dev/cu.usbserial-ST200904', false);
}
else if(configuration.environment === configuration.CONFERENCE_ROOM){
  logger.info('START Loading conference room devices...');
  //
  // CONFERENCE ROOM
  //
  // /dev/ttyUSB0 = 1 :: DISPLAY
  // /dev/ttyUSB1 = 2 :: HDMISWITCH
  // /dev/ttyUSB2 = 3 :: unused
  // /dev/ttyUSB3 = 4 :: unused
  // /dev/ttyUSB4 = 5 :: unused
  // /dev/ttyUSB5 = 6 :: unused
  // /dev/ttyUSB6 = 7 :: unused
  // /dev/ttyUSB7 = 8 :: unused
  addSamsungDisplay('conferenceroom:display', 'QM85D', '/dev/ttyUSB0');
  addIogearHDMISwitch('conferenceroom:hdmiswitch', '/dev/ttyUSB1');
  
  logger.info('END Loading conference room devices.');
}
else{
  logger.info('START Loading hackathon room devices...');
  //
  // HACKATHON ROOM
  // Darn: the order of the assigned USB's do not equal to the
  //       order on the hub. Annoying. The following were empirically
  //       determined:
  //   /dev/ttyUSB0  = port 2  : DISPLAY IR BOTTOM
  //   /dev/ttyUSB1  = port 1  : DISPLAY IR TOP
  //   /dev/ttyUSB2  = port 4  : DISPLAY FRONT RIGHT
  //   /dev/ttyUSB3  = port 9  : unused
  //   /dev/ttyUSB4  = port 3  : DISPLAY FRONT LEFT
  //   /dev/ttyUSB5  = port 12 : unused
  //   /dev/ttyUSB6  = port 6  : unused
  //   /dev/ttyUSB7  = port 11 : HDMISWITCH IR TOP
  //   /dev/ttyUSB8  = port 5  : DISPLAY SURFACEHUB
  //   /dev/ttyUSB9  = port 14 : HDMISWITCH FRONT LEFT
  //   /dev/ttyUSB10 = port 8  : USBSWITCH CAMERA FLOOR (speakers)
  //   /dev/ttyUSB11 = port 13 : HDMISWITCH IR BOTTOM
  //   /dev/ttyUSB12 = port 7  : USBSWITCH KEYBOARD MOUSE
  //   /dev/ttyUSB13 = port 16 : HDMISWITCH SURFACEHUB
  //   /dev/ttyUSB14 = port 10 : unused
  //   /dev/ttyUSB15 = port 15 : HDMISWITCH FRONT RIGHT
  
  //DISPLAYS 
  addSamsungDisplay('informationradiator:top', 'QM65F', '/dev/ttyUSB1');
  addSamsungDisplay('informationradiator:bottom', 'QM55F', '/dev/ttyUSB0'); 
  addSamsungDisplay('front:left', 'QM65F', '/dev/ttyUSB4');
  addSamsungDisplay('front:right', 'QM65F', '/dev/ttyUSB2');
  
  //SURFACEHUB
  addSurfacehub('/dev/ttyUSB8');

  //IOGEAR HDMI SWITCHES
  addIogearHDMISwitch('informationradiator:top', '/dev/ttyUSB7');
  addIogearHDMISwitch('informationradiator:bottom', '/dev/ttyUSB11');
  addIogearHDMISwitch('front:left', '/dev/ttyUSB9');
  addIogearHDMISwitch('front:right', '/dev/ttyUSB15');
  addIogearHDMISwitch('surfacehub', '/dev/ttyUSB13');

  //SMARTAVI USB SWITCHES
  addSmartAVIUSBSwitch('usbswitch:keyboard_mouse', '/dev/ttyUSB12', true);
  addSmartAVIUSBSwitch('usbswitch:camera_mics_speakers', '/dev/ttyUSB10', false);
  
  
  //TESTING
  //addSamsungDisplay('conferenceroom', 'QM85D', '/dev//dev/cu.usbserial-AL00ZD8A');
  //addIogearHDMISwitch('surfacehub', '/dev/cu.usbserial');
  //addIogearHDMISwitch('informationradiator:top', '/dev/cu.usbserial');
  //addIogearHDMISwitch('informationradiator:bottom', '/dev/cu.usbserial');
  //addIogearHDMISwitch('front:left', '/dev/cu.usbserial');*/
  //addIogearHDMISwitch('front:right', '/dev//dev/cu.usbserial-ST200912');

  logger.info('END Loading conference room devices.');
}


/*
 * 
 * 
 * 
 * 
 * 
 */
//The global model (truth). This model is derived on the fly from the current
//model of each device. In turn, these models are only updated from read status
//events within the device drivers themselves.
global.getCurrentModel = function(){
  return _.reduce(_.values(global.allDevices), function(memo, dev){
    memo[dev._key] = global.serializeDevice(dev);
    return memo;
  }, {});
};



/*
 * 
 * 
 * Talk to the client angular app through socket.io
 * 
 * 
 */

module.exports = function (socket) {

  //react to model changes on each individual device by emitting
  //a model change to the browser.
  var listeners = [];
  _.each(_.values(global.allDevices), function(dev){
    var mcl = dev.addChangeListener(function(newmodel){
      socket.emit('modelchange', global.getCurrentModel());
    });
    listeners.push(mcl);
  });
  
  //react to errors
  var activeErrorListeners = [];
  _.each(_.values(global.allDevices), function(dev){
    var ecl = dev.addErrorListener(function(isFatal, errorDescription){
      socket.emit('erroroccurred', {
        isFatal: isFatal,
        errorDescription: errorDescription
      });
    });
  });
  
  //listen for requests to change device state (e.g., input source, power, etc).
  //function takes an object with keys 'devicekey', 'set', 'value'. Examples:
  //    { devicekey: 'usbswitch_camera_mics_speakers', set: 'input', value: 3 }
  //    { devicekey: 'hdmiswitch_informationradiator_top', set: 'input', value: 2 }
  //    { devicekey: 'hdmiswitch_informationradiator_bottom', set: 'input', value: 2 }
  //    { devicekey: 'hdmiswitch_surfacehub', set: 'input', value: 1 }
  socket.on('setdevicestate', function (state_change_obj) {
    logger.info(
      'socket.js: setdevicestate called from client: %j', state_change_obj
    );
    
    var dev = global.allDevices[state_change_obj.devicekey];
    if (!dev){
      logger.error(
        'Invalid configuration. Client passed request to change device state with invalid '+
        'key: "%s". Full state_change_obj = %j', state_change_obj.devicekey, state_change_obj
      );
      socket.emit('erroroccurred', {
        isFatal: false,
        errorDescription: 'Invalid configuration. Unknown device "'+state_change_obj.devicekey+'"'
      });
      return;
    }
    
    //call the objects set method
    dev.addTask({
      name: state_change_obj.set, 
      type: 'write',
      value: state_change_obj.value
    });
  });
  
  
  //initialize the browser with the current model
  logger.info('New client connected. Current model: %j', global.getCurrentModel());
  socket.emit('init', global.getCurrentModel());
  
  // clean up when a user leaves
  socket.on('disconnect', function () {
    logger.info('client disconnected. cleaning up.');
    _.each(listeners, function(onchangefn){
      onchangefn(); //remove change listener
    });
    _.each(activeErrorListeners, function(onerrorfn){
      onerrorfn(); //remove error change listener
    });
  });
  
};
