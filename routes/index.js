var express = require('express');
var router = express.Router();
var configuration = require('./configuration')();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (configuration.environment === configuration.CONFERENCE_ROOM){
    res.render('conferenceroom_index');
  }
  else{
    res.render('hackathonroom_index');
  }
});

module.exports = router;
