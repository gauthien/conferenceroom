
var _ = require('lodash');
var serialport = require('serialport');
var readline = serialport.parsers.Readline;
var configuration = require('./configuration')();

/******************************************************************
 * 
 * The device driver for communication with the IOGear HDMI Switch GHSW8241
 * Implements the required API defined in device.js.
 * 
 ******************************************************************/
module.exports = function (hdmiswitch_iogear_GHSW8241) {
  var logger = configuration.logger;
  var drivername = 'IOGear GHSW8241';
  logger.info('%s module.exports.', drivername);
  
  return function(namekey, driverData){
    /*
     * 
     * HELPER FUNCTIONS
     * 
     */
    var commandFromTask = function(task){
      var cmd = 'read \r';
      if (task.type === 'write'){
        cmd = 'sw i0'+task.value+' \r';
      }
      return cmd;
    };
    
    /*
     * 
     * ADDITIONAL VARIABLES REQUIRED TO CREATE A DEVICE
     * 
     */
    
    //all parameters for the port
    var portParams = {
      baudRate: 19200,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      parser: new readline()
      //parser: serialport.parsers.readline('\n')
    };
    
    
    //poll for power and input
    var pollingTaskset = [
      {name: 'input', type:'read'}
    ];
    
    
    //model is considered initialized if it contains an input attribute
    var isModelInitializedFn = function(model){
      return _.isNumber(model.input);
    };
    

    //function for reading the data returned by the serial port. must return an
    //object that can contain 4 properties: 
    //      taskComplete:  boolean value as to whether the task input is finished.
    //      updatedKey:    the key for the model property to be set.
    //      updatedValue:  the value that should be set for the model[updatedKey] property.
    //      errorOccurred: if an error occurs, this object will be set and contain two
    //                     properties: isFatal, description
    var dataReceivedFn = function(data, task, currentModel){
      var toreturn = {
        taskComplete: false,
        updatedKey: undefined,
        updatedValue: undefined,
        errorOccurred: undefined
      };
      
      data = data.trim();
      logger.trace('%s data received (after trim): %s', namekey, data);

      if (data.indexOf('Command ') !== -1){ //command finished processing
        logger.debug('%s command received: %s', namekey, data);
        if (data.indexOf('Command incorrect') !== -1){
          logger.error(
            '%s error processing task "%s.%s". Command incorrect: "%s"', 
            namekey, task.name, task.type, data
          );
          toreturn.errorOccurred = {
            isFatal: false,
            errorDescription: 'Incorrect command seen on switch '+namekey+': '+data
          };
        }
        toreturn.taskComplete = true;
      }
      else if (data.indexOf('Input:port') !== -1){
        //the abstract device that the input may have changed. It will compare and 
        //update/notify listeners if needed
        logger.trace('%s input command received: %s', namekey, data);
        var idx = data.indexOf('Input:port');
        toreturn.taskComplete = true;
        toreturn.updatedKey = 'input';
        toreturn.updatedValue = +data.substring(idx+11, idx+12); //simple parse
      }
      return toreturn;
    };
    
    
    /*
     * 
     * The driver API
     * 
     */
    return {
      getCommandFromTask: commandFromTask,
      
      getPortParams: function(){
        return portParams;
      },
      
      getPollingTaskset: function(){
        return pollingTaskset;
      },
      
      dataReceived: function(data, task, currentModel){
        return dataReceivedFn(data, task, currentModel);
      },
      
      isModelInitialized: function(model){
        return isModelInitializedFn(model);
      }
    };
  };
};
