/**
 * This is an abstract class that represents a device in the AV tree. It provides
 * an abstract base for issuing, sending commands, and updating models.
 * Author: Nicholas Gauthier
 * Version: 0.1
 * Date: Nov 21, 2016
 */
var _ = require('lodash');
var serialport = require('serialport');
var readline = serialport.parsers.Readline;
var async = require("async");
var configuration = require("./configuration")();

//export function for listening to the socket
module.exports = function (device) {
  var logger = configuration.logger;
  
  
  /*
   * This is the meat of the abstract object.
   */
  var deviceCommunicator = function(namekey, deviceInfo, deviceDriver){
    logger.info('Initializing "%s"', namekey);
    
    //var parser = serialport.parsers.readline('\n');
    var parser = new readline();
    if (deviceDriver.getPortParams().parser){
      parser = deviceDriver.getPortParams().parser;
    }
    
    //serialport interface for communication device
    var rawport = new serialport(deviceInfo.interfaceId, {
      baudRate: deviceDriver.getPortParams().baudRate,
      dataBits: deviceDriver.getPortParams().dataBits,
      stopBits: deviceDriver.getPortParams().stopBits,
      parity: deviceDriver.getPortParams().parity
      //parser: parser
    });
    rawport.pipe(parser);
    

    //callback functions executed with model changes
    var listeners = [];
    var errorListeners = [];
    var modelInitListeners = [];

    //model is an abstract object:
    var model = {}; 
    
    //setup function to notify of errors
    var notifyErrorListeners = function(isFatal, message){
      _.each(errorListeners, function(listenercallback){
        listenercallback(isFatal, message);
      });
    };
    
    // The serialportDataReceived(data) is executed from the serialport.on('data')
    // function and is created within body of the task execution of the queue, allowing
    // it to have access to the task last executed and data returned. 
    // 
    // Although confusing, this workflow allows us to guarantee that only 
    // a single command-response is ever currently active on the serial port. 
    // 
    // Why not just setup "serialport.on('data')" from within this function?
    // Because the .on('data') method cannot be canceled and would end up being a
    // memory leak
    var serialportDataReceived = null;

    //Setup a queue for processing serial port commands. Note that
    //concurrency is set to 1, so any task must go to completion before
    //another task is processed.
    var q = async.priorityQueue(function (task, callback) { 
      
      //function called whenever there is data sent from the device's serial port
      serialportDataReceived = function(data){
        var datatoprint = data;
        if (_.isString(data)){ datatoprint = data.trim(); }
        logger.debug(
          '%s:"%s.%s" received data "%s" (trimmed)', namekey, task.name, task.type, datatoprint
        );
        
        //ask the non-abstract device whether the data received finalized the last request
        var dataResults = deviceDriver.dataReceived(data, task, _.clone(model));
        logger.trace('dataResults returned %j. Model is %j', dataResults, model);
        
        if (dataResults.errorOccurred){
          notifyErrorListeners(
            dataResults.errorOccurred.isFatal, 
            dataResults.errorOccurred.description
          );
        }
        else if (!_.isUndefined(dataResults.updatedValue) &&
                 !_.isNull(dataResults.updatedValue) &&
                 !_.isEqual(dataResults.updatedValue, model[dataResults.updatedKey])){
          
          var oldValue = model[dataResults.updatedKey];
          model[dataResults.updatedKey] = dataResults.updatedValue;
          logger.info(
            '%s model updated. Task %j. Updating %s: Model was "%s" and is now "%s"', 
            namekey, task, dataResults.updatedKey, oldValue, dataResults.updatedValue
          );
          
          //is the model fully initialized? only inform listeners when so.
          if(deviceDriver.isModelInitialized(model)){ 
            logger.debug('Model %j is intialized. Informing %s listeners.', model, listeners.length);
            
            if (modelInitListeners.length > 0){ //notify initListeners first if there are any
              var initListeners = _.clone(modelInitListeners);
              modelInitListeners = [];
              _.each(initListeners, function(cb){
                cb(model);
              });
            }
            
            _.each(listeners, function(cb){ //notify all other listeners of model update
              cb(task.name, data, oldValue, model);
            });
          }
        }
        
        logger.debug('%s finished processing data results: %j', namekey, dataResults);
        if (dataResults.taskComplete){
          try{
            callback();
          }catch (ex) {
            //silently catch errors--TODO: figure out when this occurs.
          }
        }
      };
      
      //check if the task is ready to execute
      if (_.isFunction(deviceDriver.abortTask) && deviceDriver.abortTask(task, model)){
        //logger.info('%s Aborting task: %j. Model: %j', namekey, task, model);
        callback();
        return;
      }
      
      //issue the command to the serialport
      var command = deviceDriver.getCommandFromTask(task);
      var cmdstr = command.cmd ? command.cmd : command; //getCommandFromTask can return an object
      logger.debug(
        '%s executing command "%s" (trimmed)', namekey, 
        _.isString(cmdstr) ? cmdstr.trim() : cmdstr
      );
      rawport.write(cmdstr);
      
      if(command.followWithTasks){
        _.each(command.followWithTasks, function(tsk){
          q.push(tsk, 0);
        });
      }
      
      if(command.immediateCompletion){
        setTimeout(callback, 50);
      }
      else{
        //catchall error to end task and allow other execution if the task doesn't 
        //complete (as expected).
        setTimeout(function(){
          try{
            callback();
            logger.error(
              '%s command "%s.%s" did not complete in %sms. Forced queue to continue processing.', 
              namekey, task.name, task.type, configuration.command_failed_time_ms
            );
          } catch (ex) {
            //this is expected--if the task completes successfully and callback
            //was previously executed, than calling callback() will throw an
            //exception.
          }
        }, configuration.command_failed_time_ms);
      }
    }, 1); //1 = concurrency of 1
    
    
    //listen for responses from device
    parser.on('data', function (data) {
      if (serialportDataReceived){
        serialportDataReceived(data);
      }
    });
    
    //a standardized function that gets called every few seconds to check 
    //the status of the device.
    var readstatusFn = function(){
      _.each(deviceDriver.getPollingTaskset(), function(task, idx){
        q.push(task, 10); //10 == low priority
      });
    };
    
    // anytime all the queue is finished, execute read status 
    q.drain = function(){
      setTimeout(readstatusFn, configuration.poll_frequency_ms); 
    };
    
    //startup initial status reading
    rawport.on('open', function(data){ 
      logger.info('%s port opened.', namekey);
      readstatusFn();
    });
    
    //startup or other read errors
    rawport.on('error', function(err) {
      logger.error('Error opening %s port "%s": ', namekey, err.message);
      notifyErrorListeners(true, 'Error opening '+namekey+' port: ' + err.message);
    });
    
    return {
      /*
       * Returns the current model
       */
      getInfo: function(){
        return deviceInfo;
      },
      
      
      /*
       * Returns the current model
       */
      getModel: function(){
        return model;
      },
      
      /*
       * Add a task to the queue with a very high execution priority. Will be executed
       * next if any polling tasks are executing, or in order if another task has been
       * added through this function.
       * 
       * The task object can contain any properties that you want, but must contain the
       * following two properties:
       *   'name':       The name of this task (e.g., input, power). This name must be the
       *                 name of the object property being written or read.
       *   'type':       The type of the task (e.g., read / write). Not used by this
       *                 abstract class other than for logging purposes.
       *   'value':      Optional, but specify the value for a write request 
       */
      addTask: function(task){
        q.push(task, 0);
      },
      
      /* 
       * Any change to the device, the callback will be executed. This 
       * callback function should have the signature: 
       *    fn(changetype, newvalue, oldvalue, model)
       * 
       * where: 'changetype' will be 'input' or 'power'
       *        'newvalue' will contain the value of the new setting
       *                      (integers 0 or 1 for 'power')
       *                      ('HDMI1', 'HDMI2', or 'DISPLAYPORT for 'input')
       *        'oldvalue' will contain the previously set value
       *        'model' will contain the full model object, which has two
       *                keys 'input' and 'power'
       * 
       * returns: a function that, when executed, will remove the callback from
       *          future notifications.
       */
      addChangeListener: function(callback){
        logger.error('addChangeListener called!');
        listeners.push(callback);
        return function(){
          listeners = _.reduce(listeners, function(memo, item){
            if (item != callback){ memo.push(item); }
            return memo;
          }, []);
        };
      },
      
      /*
       * Any detected error will result in this callback being executed.
       * The callback function should have the signature: 
       *   fn(isFatal, description)
       * 
       * where: 'isFatal' reports whether error is non-recoverable.
       *        'description' will be a user readable string describing the error.
       *        
       * returns: a function that, when executed, will remove the callback from
       *          future notifications.
       */
      addErrorListener: function(callback){
        errorListeners.push(callback);
        return function(){
          errorListeners = _.reduce(errorListeners, function(memo, item){
            if (item != callback){ memo.push(item); }
            return memo;
          }, []);
        };
      },
      
      /*
       * An internal function that will be executed exactly one time once the model 
       * is initialized. If the model is already initialized, the callback will be 
       * immediately called.
       */
      addInitializedListener: function(callback){
        if (deviceDriver.isModelInitialized(model)){
          return callback(model);
        }
        modelInitListeners.append(callback);
      },
      
      
      /*
       * 
      getSerialized: function(){
        return {
          type: deviceInfo.type,
          manufacturer: deviceInfo.manufacturer,
          modelnum: deviceInfo.modelnum,
          location: deviceInfo.location,
          model: model
        };
      }
      */
    }
  };
  
  
  var allDevices = {};
  return {
    /*
     * Get an instance of a particular device. If already initialized, will return immediately,
     * otherwise will open port and initiate loading.
     * 
     * Parameters:
     *   deviceInfo:   An object containing the following required parameters:
     *       type:         The type of the device (e.g., hdmiswitch, usbswitch, display, etc)
     *       manufacturer: The manufacturer of the device (e.g., smart-avi, samsung, iogear, etc)
     *       modelnum:     The model number of the device (e.g., GHSW8241, HDN-8P, etc)
     *       location:     The location of the item that this device controls (e.g., frontleft, 
     *                     frontright, surfacehub, )
     *       interfaceId: The interfaceId of this particular device (i.e., serialport/usb id)
     *       
     *   driverData:   An object that will be provided to the device driver. Can provide 
     *                 additional context to the drivers and will be passed to the constructor
     *                 functions upon instantiation.
     *   
     *   deviceDriver: A device driver object that implements the following functions:
     *       abortTask(task, model):
     *           optional function that will be called before executing any task is executed
     *           in order to allow certain tasks to not be executed if an number of conditions are
     *           met (e.g., with samsung: if power is off, don't query for input)
     *       
     *       getCommandFromTask(task):
     *           should return the actual command that will be passed to the serialport 
     *           given the provided task. Optional, return an object with the following
     *           properties:
     *             cmd:                  the actual command to execute
     *             followWithTasks:      an array of tasks to follow execution of the command
     *             immediateCompletion:  a boolean that if true, will immeditatlly finalize
     *                                   this task and execute the next command in the queue.
     *           
     *       getPortParams():     
     *           should return an object containing all required parameters to open 
     *           the port. This should include keys for 'baudRate', 'dataBits', 
     *           'stopBits', 'parity', and 'parser'.
     *           
     *       getPollingTaskset():
     *           Should return an array of tasks to be executed repeatedly. Normally 
     *           a set of read commands to make sure the internal state matches the 
     *           current device status.
     *           
     *       dataReceived(data, task, currentModel):
     *           A function that will be called whenever data is received from the 
     *           port. The function should accept 3 parameters: data, task, and 
     *           currentModel (a copy of the current model). This function should 
     *           return an object that contains 3 parameters:
     *               'updatedKey':    If the data received will update a model value, 
     *                                this parameter should contain the model key that 
     *                                should be updated (i.e.,  model[updatedKey])
     *               'updatedValue':  If the data received will update a model value, 
     *                                this parameter should contain the value. Ignored 
     *                                if undefined. Otherwise, it will be compared with 
     *                                the current model[updatedKey] and if not equal, 
     *                                the model will be updated and listeners will be 
     *                                notified.
     *               'taskComplete':  a boolean that indicates whether the task is 
     *                                completed. If true, the queue will continue to 
     *                                execute.
     *               'errorOccurred': if an error occurs, this object should be set and 
     *                                contain two properties: isFatal, description
     *                               
     *       isModelInitialized(model): 
     *           A function that will be passed a model and should return whether or
     *           not the model contains all data required to be considered initialized.
     * 
     * Returns: An single 'deviceCommunicator' object that contains functions 'getModel', 
     *          'addTask', 'addChangeListener', and 'addErrorListener'. Documentation for 
     *          how to call these functions is available above.
     *   
     */
    getDevice: function(deviceInfo,
                        driverData,
                        deviceDriver){
      
      logger.trace(
        'getDevice called for device %s :: %s', deviceInfo.deviceName, deviceInfo.interfaceId
      );
      
      var key = deviceInfo.type+'_'+deviceInfo.manufacturer+'_'+
                deviceInfo.model+'_'+deviceInfo.location+'['+deviceInfo.interfaceId+']';
      
      var toreturn = allDevices[key];
      if (toreturn){ //device already initialized
        logger.info('Returning device %s from cache', key);
        return toreturn;
      }
      
      logger.info(
        'Loading new device %s: ', key
      );
      
      var toreturn = deviceCommunicator(key, deviceInfo, deviceDriver(key, driverData));
      allDevices[key] = toreturn;
      return toreturn;
    }
  };
};
